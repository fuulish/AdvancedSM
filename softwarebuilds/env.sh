ENV_BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

export PATH=$ENV_BASE_DIR/bin:$ENV_BASE_DIR/tools:$ENV_BASE_DIR/tools:$PATH
export INCLUDE=$ENV_BASE_DIR/include:$INCLUDE
export C_INCLUDE_PATH=$ENV_BASE_DIR/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=$ENV_BASE_DIR/include:$CPLUS_INCLUDE_PATH
export LIBRARY_PATH=$ENV_BASE_DIR/lib:$ENV_BASE_DIR/lib64:$LIBRARY_PATH
export LD_LIBRARY_PATH=$ENV_BASE_DIR/lib:$ENV_BASE_DIR/lib64:$LD_LIBRARY_PATH
export MANPATH=$ENV_BASE_DIR/share/man:$MANPATH
export PYTHONPATH=$ENV_BASE_DIR/lib64/python2.7/site-packages:$ENV_BASE_DIR/lib/python2.7/site-packages:$PYTHONPATH
export GPAW_SETUP_PATH=$ENV_BASE_DIR/gpaw-setups-0.9.20000

unset ENV_BASE_DIR

alias iso='gpaw-python -m ase.visualize.mlab -C gpaw'
