# General #

- more (any) comments in the input files explaining the setup
- info on Atomic Simulation Environment and calculators
    * then in particular about gpaw/cp2k/octopus (whichever we will use in the end)
- clearer distinction between LCAO-MO and grid-calculations
- clearer distinction/clarification between/of vdw-DF2 and TS09
- check density decay LDA/SIC-LDA, PBE/SIC-PBE/PBE0 (it's not all that simple and consistent)
- atomization energies for hydrogen molecule also with PBE0 and PBE --> is SIC needed here?
- analytical LDA/LSD comparison for kinetic and exchange energy (see infamous book)
- explain smearing and angular quantum number in dos.py/project_dos.py
