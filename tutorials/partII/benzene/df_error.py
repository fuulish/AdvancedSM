#!/usr/bin/env gpaw-python

from __future__ import print_function
import numpy as np

from ase.io import write, read
from ase.parallel import *
from ase.visualize import view
from ase.structure import molecule
from gpaw import GPAW, PW
from gpaw import setup_paths
from gpaw.xc.vdw import VDWFunctional
from gpaw.xc.hybrid import HybridXC

from ase.io import Trajectory

benz1 = molecule('C6H6')
benz2 = benz1.copy()

dimer = benz1 + benz2
dimer = read('sherrill2004sandwich.xyz')
dimer.center(vacuum=3.0)
dimer.set_pbc(False)

monom = dimer[:12]
monom.set_cell(dimer.get_cell())
monom.set_pbc(False)

xc = 'vdW-DF2'
h = 0.30
En = -0.07376235900131325

hyb = HybridXC('EXX', finegrid=False)
vdwxc = VDWFunctional('vdW-DF2')

vdwdimer = GPAW(xc=vdwxc, spinpol=False, mode='fd', h=h, eigensolver='rmm-diis', txt='dimerVDW.out', charge=0,)
vdwmonom = GPAW(xc=vdwxc, spinpol=False, mode='fd', h=h, eigensolver='rmm-diis', txt='monomVDW.out', charge=0,)

dimer.set_calculator(vdwdimer)
monom.set_calculator(vdwmonom)

En_dimer = dimer.get_potential_energy()
En_monom = monom.get_potential_energy()

intervdw = En_dimer - 2*En_monom

parprint('vdW interaction energy: %14.8f' %intervdw)

nrmlxc = 'PBE'
dcalc = GPAW(xc=nrmlxc, mode='fd', h=h, eigensolver='rmm-diis', txt='dimerPBE.out', charge=0,)
mcalc = GPAW(xc=nrmlxc, mode='fd', h=h, eigensolver='rmm-diis', txt='monomPBE.out', charge=0,)

dimer.set_calculator(dcalc)
di = dimer.get_potential_energy()

monom.set_calculator(mcalc)
mo = monom.get_potential_energy()

Etnt = di - 2*mo

dit = vdwdimer.get_xc_difference(nrmlxc)
mot = vdwmonom.get_xc_difference(nrmlxc)

din = dimer.calc.get_xc_difference(vdwxc)
min = monom.calc.get_xc_difference(vdwxc)

parprint('vdW on top of regular density: %14.8f' %(Etnt + din - 2*min))

Etn = intervdw + dit - 2*mot

parprint('vdw interaction: %14.8f, ... on top of vdw: %14.8f, density error: %14.8f, functional error: %14.8f, total error: %14.8f'  %(intervdw, Etn, Etnt - Etn, Etn - En, Etnt - En))
