#!/usr/bin/env gpaw-python

from __future__ import print_function
import numpy as np

from ase.io import write, read
from ase.parallel import *
from ase.visualize import view
from ase.structure import molecule
from gpaw import GPAW, PW
from ase.calculators.vdwcorrection import vdWTkatchenko09prl
from gpaw.analyse.hirshfeld import HirshfeldPartitioning
from gpaw.analyse.vdwradii import vdWradii

from ase.io import Trajectory

xc = 'PBE'
h = 0.30

calc = GPAW(xc=xc, mode='fd', h=h) #, eigensolver='rmm-diis')

dstncr = 0.5

dimer_en = []
monom_en = []

disp_dimer_en = []
disp_monom_en = []

inter = []
disp_inter = []

traj = Trajectory('dimer.traj', 'w')
efl = paropen('fd_energy.dat', 'w')
disp_efl = paropen('fd_disp_energy.dat', 'w')

dimer = read('sherrill2004sandwich.xyz')
com = dimer.get_center_of_mass()

for a in range(len(dimer)):
    dimer.positions[a,:] -= com

for dst in np.append(np.arange(3.0, 4.1, 0.2), np.arange(4.5, 5.51, 0.5)):
    dimer.positions[:12,1] = -dst/2.
    dimer.positions[12:,1] =  dst/2.

    dimer.center(vacuum=3.0)
    dimer.set_pbc(False)

    benz1 = dimer[:12]

    dimer.set_calculator(calc)
    dimer_en.append(dimer.get_potential_energy())

    cc = vdWTkatchenko09prl(HirshfeldPartitioning(calc),vdWradii(dimer.get_chemical_symbols(), 'PBE'))
    dimer.set_calculator(cc)
    disp_dimer_en.append(dimer.get_potential_energy())

    benz1 = dimer[:12]
    benz1.positions[:,:] = dimer.positions[:len(benz1),:]
    benz1.set_cell(dimer.get_cell())
    benz1.set_pbc(False)
    benz1.set_calculator(calc)
    monom_en.append(benz1.get_potential_energy())

    cc = vdWTkatchenko09prl(HirshfeldPartitioning(calc),vdWradii(benz1.get_chemical_symbols(), 'PBE'))
    benz1.set_calculator(cc)
    disp_monom_en.append(benz1.get_potential_energy())

    inter.append(dimer_en[-1] - 2.*monom_en[-1])
    disp_inter.append(disp_dimer_en[-1] - 2.*disp_monom_en[-1])

    traj.write(dimer)
    efl.write('%14.8f %14.8f %14.8f %14.8f\n' %(dst, monom_en[-1], dimer_en[-1], inter[-1]))
    disp_efl.write('%14.8f %14.8f %14.8f %14.8f\n' %(dst, disp_monom_en[-1], disp_dimer_en[-1], disp_inter[-1]))
