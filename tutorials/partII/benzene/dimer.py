#!/usr/bin/env gpaw-python

from __future__ import print_function
import numpy as np

from ase.units import Ha
from ase.io import write, read
from ase.parallel import *
from ase.visualize import view
from ase.structure import molecule
from gpaw import GPAW, PW
from gpaw import setup_paths

setup_paths.insert(0,'.')

from ase.io import Trajectory

## benz1 = molecule('C6H6')
## benz2 = benz1.copy()
## 
## #view(benz1)
## 
## benz2.positions[:,2] += 6.0
## 
## dimer = benz1 + benz2
## 
## a = 10
## dimer.set_cell((a,a,a))
## dimer.center()
## 
## #view(dimer)

h = 0.30

xc = 'PBE'
gga_calc = GPAW(nbands=0, xc=xc, mode='fd', h=h) #, eigensolver='rmm-diis')
pertXC = 'vdW-DF2'

dimer_en = []
monom_en = []
inter = []

traj = Trajectory('dimer.traj', 'w')
efl = paropen('energy.dat', 'w')

dimer = read('sherrill2004sandwich.xyz')
com = dimer.get_center_of_mass()

for a in range(len(dimer)):
    dimer.positions[a,:] -= com

view(dimer[:12])

for dst in np.append(np.arange(3.0, 4.1, 0.2), np.arange(4.5, 5.51, 0.5)):
    dimer.positions[:12,1] = -dst/2.
    dimer.positions[12:,1] =  dst/2.

    dimer.center(vacuum=4.0)
    dimer.set_pbc(False)

    benz1 = dimer[:12]

    dimer.set_calculator(gga_calc)

    view(dimer)
    dimer_gga_en = dimer.get_potential_energy()
    dimer_xcdiff = dimer.calc.get_xc_difference(xc=pertXC)
    dimer_en.append(dimer_gga_en + dimer_xcdiff)

    benz1.set_cell(dimer.get_cell())
    benz1.set_pbc(False)
    benz1.set_calculator(gga_calc)

    benz1_gga_en = benz1.get_potential_energy()
    benz1_xcdiff = benz1.calc.get_xc_difference(xc=pertXC)
    monom_en.append(benz1_gga_en + benz1_xcdiff)

    inter.append(dimer_en[-1] - 2.*monom_en[-1])

    traj.write(dimer)
    efl.write('%14.8f %14.8f %14.8f %14.8f\n' %(dst, monom_en[-1], dimer_en[-1], inter[-1]))
