#!/usr/bin/env gpaw-python

from __future__ import print_function
import numpy as np

from ase.io import write, read
from ase.parallel import *
from ase.visualize import view
from ase.structure import molecule
from gpaw import GPAW, PW
from gpaw import setup_paths

from gpaw.xc.vdw import VDWFunctional

setup_paths.insert(0,'.')

from ase.io import Trajectory

xc = 'vdW-DF2'
dfa = VDWFunctional(xc)
h = 0.3

setups = {}
for i in range(12):
    setups[i] = 'ghost'

dcalc = GPAW(xc=dfa, mode='lcao', basis='dzp', h=h) #, eigensolver='rmm-diis')
mcalc = GPAW(xc=dfa, mode='lcao', basis='dzp', h=h, setups=setups ) #, eigensolver='rmm-diis')

#calc = GPAW(xc=dfa, mode='fd', h=h) #, eigensolver='rmm-diis')
#calc = GPAW(xc=dfa, mode=PW(200)) #, eigensolver='rmm-diis')

dstncr = 0.5
dimer_en = []
monom_en = []
inter = []

traj = Trajectory('dimer.traj', 'w')
efl = paropen('energy.dat', 'w')

dimer = read('sherrill2004sandwich.xyz')
com = dimer.get_center_of_mass()

for a in range(len(dimer)):
    dimer.positions[a,:] -= com

view(dimer[:12])

for dst in np.append(np.arange(3.0, 4.1, 0.2), np.arange(4.5, 5.51, 0.5)):
    dimer.positions[:12,1] = -dst/2.
    dimer.positions[12:,1] =  dst/2.

    dimer.center(vacuum=4.0)
    dimer.set_pbc(False)

    benz1 = dimer[:12]

    dimer.set_calculator(dcalc)
    dimer_en.append(dimer.get_potential_energy())

    view(dimer)
#BSSE calc
    dimer.set_calculator(mcalc)
    monom_en.append(dimer.get_potential_energy())

#non-BSSE calc
    #benz1.positions[:,:] = dimer.positions[:len(benz1),:]
    #benz1.set_cell(dimer.get_cell())
    #benz1.set_pbc(False)
    #benz1.set_calculator(dcalc)
    #monom_en.append(benz1.get_potential_energy())

    inter.append(dimer_en[-1] - 2.*monom_en[-1])

    traj.write(dimer)
    efl.write('%14.8f %14.8f %14.8f %14.8f\n' %(dst, monom_en[-1], dimer_en[-1], inter[-1]))

#inter = np.array(inter)
#parprint inter
#np.savetxt('energy.dat', inter)
