#!/usr/bin/env gpaw-python

import numpy as np

from gpaw import GPAW, FermiDirac
from ase import Atoms
from ase.io import read, write
from gpaw import GPAW, PoissonSolver, Mixer, PW
#from ase.structure import bulk 
from ase.lattice import bulk

from ase.parallel import parprint
from ase.visualize import view

# -------------------------------------------------------------
# Bulk configuration
# -------------------------------------------------------------

en = []

distances = np.arange(3.15, 6.1, 0.2)

for dst in distances:
    gnr = bulk('C', 'hcp', a=2.4612, c=2*dst) # hexagonal close-packed (hpc)
    gnr.positions=([[ 0. ,  0.,  dst], [ 1.23060,  0.7104873, dst ]])
    
    view(gnr)
    
    write('gr.traj', gnr)
    
    xc='vdW-DF2'
    
    # Make self-consistent calculation and save results
    calc = GPAW(
                #mode=PW(500),
                mode='fd', #finite difference(fd)
                h=0.18,
                xc=xc,
                nbands=20,
                kpts=(6,6,1),
                occupations=FermiDirac(width=0.20, maxiter=2000),
                #mixer=Mixer(beta=0.010, nmaxold=8, weight=100.0),
                poissonsolver=PoissonSolver(eps=1e-12),
                txt='/dev/null',
                )
    
    gnr.set_calculator(calc)
    en.append(gnr.get_potential_energy())
    calc.write('band_sc.gpw')
    parprint(dst, en[-1])

#for i, dst in enumerate(distances):
#    parprint(dst, en[i])
    
