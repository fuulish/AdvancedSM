#!/usr/bin/env gpaw-python

from gpaw import GPAW, FermiDirac
from ase import Atoms
from ase.io import read, write
from gpaw import GPAW, PoissonSolver, Mixer, PW
from ase.lattice import bulk

from ase.visualize import view

# -------------------------------------------------------------
# Bulk configuration
# -------------------------------------------------------------

gnr = bulk('C', 'hcp', a=2.4612, c=6.709) # hexagonal close-packed (hpc)
gnr.positions=([[ 0. ,  0.,  0.], [ 1.23060,  0.7104873,  0. ]])

view(gnr)

write('gr.traj', gnr)

xc='PBE'
xc='vdW-DF2'

# Make self-consistent calculation and save results
calc = GPAW(
            mode='fd', #finite difference(fd)
            h=0.18,
            xc=xc,
            nbands=20,
            kpts=(30,30,1),
            occupations=FermiDirac(width=0.05, maxiter=2000),
            #mixer=Mixer(beta=0.010, nmaxold=8, weight=100.0),
            poissonsolver=PoissonSolver(eps=1e-12),
            #txt='band_sc.txt',
            )

gnr.set_calculator(calc)
gnr.get_potential_energy()
calc.write('band_sc.gpw')
