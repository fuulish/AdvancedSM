#!/usr/bin/env gpaw-python

from __future__ import print_function

from ase.parallel import parprint
from math import sqrt
import numpy as np
from ase import Atoms
from ase.parallel import paropen
from gpaw import GPAW, FermiDirac, PW
from gpaw.response.df import DielectricFunction

from ase.visualize import view

en = []
distances = np.arange(3.15, 6.1, 0.2)

for dst in distances:
    a = 1.42
    c = 3.355
    c = dst
    
    # Generate graphite AB-stack structure:
    atoms = Atoms('C4',
                  scaled_positions=[(1 / 3.0, 1 / 3.0, 0),
                                    (2 / 3.0, 2 / 3.0, 0),
                                    (0, 0, 0.5),
                                    (1 / 3.0, 1 / 3.0, 0.5)],
                  pbc=(1, 1, 1),
                  cell=[(sqrt(3) * a / 2, 3 / 2.0 * a, 0),
                        (-sqrt(3) * a / 2, 3 / 2.0 * a, 0),
                        (0, 0, 2 * c)])
    
    view(atoms)
    
    xc = 'PBE'
    xc = 'vdW-DF2'
    
    calc = GPAW(mode='fd',
                xc=xc,
                h=0.18,
                kpts=(6,6, 1),
                #txt='graphite-%4.2f' %dst,
                # Use smaller Fermi-Dirac smearing to avoid intraband transitions:
                occupations=FermiDirac(0.05))
    
    atoms.set_calculator(calc)
    en.append(atoms.get_potential_energy())
    atoms.calc.write('graphite-dist-%4.2f.gpw' %dst)

for i, dst in enumerate(distances):
    parprint(dst, en[i])
