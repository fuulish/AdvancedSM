#!/usr/bin/env gpaw-python

import sys

from gpaw import GPAW
from gpaw import restart
from gpaw.utilities.dos import print_projectors
import matplotlib.pyplot as plt

print print_projectors('C')
filename = sys.argv[1]
if len(sys.argv) > 2:
    width = float(sys.argv[2])
else:
    width = None

atoms, calc = restart(filename)
ef = calc.get_fermi_level()

e, dos = calc.get_orbital_ldos(a=0, angular=0, width=width)
plt.plot(e-ef, dos, label='s')

plt.legend()
plt.xlabel(r'$\epsilon - \epsilon_F \ \rm{(eV)}$')
plt.ylabel('Density of States (1/eV)')
plt.savefig('pdos.pdf')
plt.show()
