#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt
import numpy as np

from ase import Atoms, Atom
from ase.io import write, read, Trajectory
from ase.parallel import *

from ase.visualize import view
from gpaw import GPAW, PW
from gpaw.xc.sic import SIC

#a = 6

mol = read('h3o.xyz')
#mol.set_cell((a,a,a))
mol.set_pbc((False, False, False))
mol.center(vacuum=4.0)

mol.set_pbc((False, False, False))
xc_factor = np.arange(0.8, 1.01, 0.1)

coulomb_factor = 1.0
xc = 'PBE'
h = 0.35

homo_energy = []
vie = []

for xcf in xc_factor:

    dfa = SIC(xc=xc, xc_factor=xcf, coulomb_factor=coulomb_factor)
    calc = GPAW(xc=dfa, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='neutral_xcfactor-%4.2f.out' %xcf, charge=0,)
    mol.set_calculator(calc)
    mol.set_pbc((False, False, False))
    ne = mol.get_potential_energy()

    eig = mol.calc.get_eigenvalues()
    occ = mol.calc.get_occupation_numbers()
    ndx = np.where(occ > 0.1)[0]
    homo_energy.append(eig[ndx][-1])

    calc = GPAW(xc=dfa, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='cation_xcfactor-%4.2f.out' %xcf, charge=1,)
    mol.set_calculator(calc)
    ca = mol.get_potential_energy()

    vie.append(ca - ne)

for i, ie in enumerate(vie):
    parprint('xc_factor: %4.2f VIE: %14.8f HOMO: %14.8f' %(xc_factor[i], ie, homo_energy[i]))

