#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt
import numpy as np

from ase import Atoms, Atom
from ase.io import write, read, Trajectory
from ase.parallel import *

from ase.structure import molecule
from ase.visualize import view
from gpaw import GPAW
from gpaw.xc.sic import SIC
from gpaw.xc.hybrid import HybridXC

mol = read('h3o.xyz')
mol.set_pbc(False)
mol.center()

wat = Atoms(mol[range(3)])
hyd = Atoms([mol[-1]])

h = ???
dfa = ???

en = []

traj = Trajectory('dissociate.traj', 'w', properties=['energy'])
traj.atoms = mol

for dist in np.arange(0.9, 1.31, 0.1):

    en = 0
    mol.set_distance(0, 3, dist, fix=0)
    mol.center(vacuum=3.0)

    calc = GPAW(xc=dfa,
            spinpol=True,
            mode='fd',
            h=h,
            eigensolver='rmm-diis',
            charge=0,
            basis='szp(dzp)'
            )

    mol.set_calculator(calc)
    en += mol.get_potential_energy()

    wat.positions[:] = mol.positions[[0,1,2],:]
    wat.set_cell(mol.get_cell())
    wat.set_calculator(calc)
    en -= wat.get_potential_energy()

    hyd.positions[:] = mol.positions[3,:]
    hyd.set_cell(mol.get_cell())
    hyd.set_calculator(calc)
    en -= hyd.get_potential_energy()

    traj.write(energy=en)
