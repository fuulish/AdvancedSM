#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interpolate

dat = np.loadtxt('vie_homo.dat')

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(dat[:,0], dat[:,1], 'o', color='b', label='-VIE')
ax.plot(dat[:,0], -dat[:,2], 'o', color='g', label='SOMO')

x = dat[:,0].copy()
vie = dat[:,1].copy()
som = dat[:,2].copy()

tck_vie = interpolate.splmake(x, vie, order=2)
xnew = np.linspace(x[0], x[-1], 100, endpoint=True)
vie_new = interpolate.spleval(tck_vie, xnew)

tck_som = interpolate.splmake(x, som, order=2)
xnew = np.linspace(x[0], x[-1], 100, endpoint=True)
som_new = interpolate.spleval(tck_som, xnew)

ax.plot(xnew, vie_new, color='b')
ax.plot(xnew, -som_new, color='g')

#axx = ax.twinx()

dff = np.abs(som_new + vie_new)

#axx.plot(xnew, dff, '--', color='k')

plt.show()
