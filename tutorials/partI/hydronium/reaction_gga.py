#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt
import numpy as np

from ase import Atoms, Atom
from ase.io import write, read, Trajectory
from ase.parallel import *
from ase.optimize import BFGS
from ase.constraints import FixAtoms

from ase.structure import molecule
from ase.visualize import view
from gpaw import GPAW
from gpaw.xc.sic import SIC

a = 8

mol = read('h3o.xyz')
mol.set_pbc(False)

h = 0.35
xc = 'PBE'
dfa = xc

en = []

traj = Trajectory('dissociate_gga.traj', 'w', properties=['energy'])
traj.atoms = mol

#for dist in np.arange(0.9, 1.6, 0.1):
for dist in np.array([0.9, 1.1, 1.5]):

    en = 0
    mol.set_distance(0, 3, dist, fix=0)
    mol.center(vacuum=4.0)

    calc = GPAW(xc=dfa,
            spinpol=True,
            mode='fd',
            h=h,
            eigensolver='rmm-diis',
            charge=0,
            )

    mol.set_calculator(calc)
    mol.set_constraint(FixAtoms((0,3)))

    opt = BFGS(mol, trajectory='opt-dist-%4.2f.traj' %dist)
    opt.run(fmax=0.5)
