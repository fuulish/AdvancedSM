#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt
import numpy as np

from ase import Atoms, Atom
from ase.io import write, read, Trajectory
from ase.parallel import *

from ase.visualize import view
from gpaw import GPAW, PW
from gpaw.xc.sic import SIC
from gpaw.xc.hybrid import HybridXC

mol = read('h3o.xyz')
mol.set_pbc(False)
mol.center(vacuum=4.0)

mol.set_pbc((False, False, False))
xc_factor = np.arange(0.7, 1.01, 0.1)

coulomb_factor = 1.0
xc = 'PBE'
h = 0.35

En = 5.23 #MP2//aug-cc-pVDZ VIE at minimum geometry

Etn = []
Etnt = []

hyb = HybridXC('EXX', finegrid=False)

hfneutral = GPAW(xc=hyb, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='neutral_hf.out', charge=0,)
hfcation = GPAW(xc=hyb, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='cation_hf.out', charge=1,)

hfne = mol.copy()
hfca = mol.copy()

hfne.set_calculator(hfneutral)
hfca.set_calculator(hfcation)

En_neutral = hfne.get_potential_energy()
En_cation = hfca.get_potential_energy()

hfvie = En_cation - En_neutral

parprint('Hartree Fock VIE: %14.8f' %hfvie)

xc_factor = 0.9
dfas = [HybridXC('PBE0', finegrid=False), SIC(xc=xc, xc_factor=xc_factor, coulomb_factor=coulomb_factor), 'PBE']
lbls = ['PBE0', 'PBE-SIC-OPT', 'PBE']

for nm, xc in zip(lbls, dfas):

    neutral_calc = GPAW(xc=xc, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='neutral_%s.out' %nm, charge=0,)
    cation_calc = GPAW(xc=xc, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='cation_%s.out' %nm, charge=1,)

    mol.set_calculator(neutral_calc)
    ne = mol.get_potential_energy()

    mol.set_calculator(cation_calc)
    ca = mol.get_potential_energy()

    Etnt.append(ca - ne)

    ne = hfne.calc.get_xc_difference(xc)
    ca = hfca.calc.get_xc_difference(xc)

    Etn.append(hfvie + ca - ne)

for i, ie in enumerate(Etnt):
    parprint('xc_factor: %s, VIE: %14.8f, VIE on top of HF density %14.8f, density error: %14.8f, functional error: %14.8f, total error: %14.8f' %(lbls[i], Etnt[i], Etn[i], Etnt[i] - Etn[i], Etn[i] - En, Etnt[i] - En))

