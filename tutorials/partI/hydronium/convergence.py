#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt
import numpy as np

from ase.parallel import *
from ase import Atoms, Atom
from ase.io import read

from gpaw import GPAW, PW
from gpaw.xc.sic import SIC
from gpaw.xc.hybrid import HybridXC

mol = read('h3o.xyz')
mol.set_pbc((False, False, False))
mol.center(vacuum=3.0)

xc = 'PBE'
dfa = ???

vie = []

#0.20 spacing alone takes about 15 minutes on 4 cores <--- almost as long as the rest and no real gain in accuracy
spacings = np.arange(0.25, 0.41, 0.05)

for h in spacings:

    calc = GPAW(xc=dfa, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='neutral-%4.2f.out' %h, charge=0,) 
    mol.set_calculator(calc)
    ne = mol.get_potential_energy()

    calc = GPAW(xc=dfa, spinpol=True, mode='fd', h=h, eigensolver='rmm-diis', txt='cation-%4.2f.out' %h, charge=1,) 
    mol.set_calculator(calc)
    ca = mol.get_potential_energy()

    vie.append(ca - ne)

for h, e in zip(spacings, vie):
    parprint('spacing: %4.2f and energy: %14.8f' %(h, e))

