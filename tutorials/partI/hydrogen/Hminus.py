#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt

import numpy as np
from ase import Atoms, Atom
from ase.io import write, read
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.optimize import QuasiNewton, MDMin
from ase.neb import NEB
from ase.units import Bohr, Ha

from gpaw import GPAW, PoissonSolver
from gpaw import PW
from gpaw.xc.sic import SIC
from gpaw.xc.hybrid import HybridXC

xc = 'EXX'
prx = 'PBE'

a = 6.0
spacing = 0.15
mol = Atoms('H', magmoms=[0], cell=(a, a, a))
mol.center()

dfa = HybridXC(xc, finegrid=False)
calc = GPAW(xc=dfa, spinpol=True, mode='fd', h=spacing, eigensolver='rmm-diis', charge=-1)

mol.set_calculator(calc)

refen_calc = mol.get_potential_energy()
refen_calc += mol.calc.get_reference_energy()

en = -13.6 - 0.754 #(taken from wiki page about electron affinities

etn = refen_calc + mol.calc.get_xc_difference(xc=prx)
errorfunc = etn - en

mol.calc.set(xc=prx)

etnt = mol.get_potential_energy()
etnt += mol.calc.get_reference_energy()

errordens = etnt - etn

print("total error: %14.8f, density error: %14.8f, functional error: %14.8f" %(etnt - en, errordens, errorfunc))
