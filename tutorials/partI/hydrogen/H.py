#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt

import numpy as np
from ase import Atoms, Atom
from ase.io import write
from ase.visualize import view
from ase.units import Bohr
from ase.parallel import *

from gpaw import GPAW
from gpaw.xc.sic import SIC

a = 6.0
atom = Atoms('H', magmoms=[1.0], cell=(a, a, a), pbc=(False, False, False))
atom.center()

xc_factor = 1.0
coulomb_factor = 1.0

func = ['LDA', SIC(xc='LDA', xc_factor=xc_factor, coulomb_factor=coulomb_factor), 'PBE', SIC(xc='PBE', xc_factor=xc_factor, coulomb_factor=coulomb_factor), 'PBE0']
lbls = ['LDA', 'LDA-PZ-SIC', 'PBE', 'PBE-PZ-SIC', 'PBE0']

en = []

spacing = 0.15

for nm,dfa in zip(lbls, func):
    mol = atom.copy()

    calc = GPAW(xc=dfa, spinpol=True, mode='fd', h=spacing, txt='out.%s.txt' %nm, eigensolver='rmm-diis')

    parprint("running for ", nm)
    mol.set_calculator(calc)
    en.append(mol.get_potential_energy() + calc.get_reference_energy())

    density = calc.get_all_electron_density()
    write('density_%s.cube' %nm, mol, data=density*Bohr**3)

for dfa, energy in zip(lbls, en):
    parprint('%10s: %14.8f' %(dfa, energy))
