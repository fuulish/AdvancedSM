#!/usr/bin/env gpaw-python

from __future__ import print_function

import numpy as np

from ase.parallel import *
from ase import Atoms, Atom
from ase.parallel import paropen as open
from gpaw import GPAW
from gpaw.xc.sic import SIC

a = 6.  # Size of unit cell (Angstrom)
c = a / 2

# Hydrogen atom:
atom = Atoms('H',
             positions=[(c, c, c)],
             magmoms=[1],
             cell=(a, a + 0.0001, a + 0.0002))  # Break cell symmetry

# Hydrogen molecule:
d = 0.74  # Experimental bond length
molecule = Atoms('H2',
                 positions=([c - d / 2, c, c],
                            [c + d / 2, c, c]),
                 cell=(a, a, a))
    
xc = 'PBE'
xc_factor=1.0
coulomb_factor=1.0

exactresult = 4.52

fd = open('atomization.txt', 'w')

prms = np.arange(0.8, 1.01, 0.1)

for xc_factor in prms:
    
    dfa = SIC(xc, xc_factor=xc_factor, coulomb_factor=coulomb_factor)
    
    # gpaw calculator:
    calc = GPAW(mode='fd',
                h=0.10,
                xc=dfa,
                spinpol=True,
                eigensolver='rmm-diis',  # This solver can parallelize over bands
                )

    atom.set_calculator(calc)
    
    e1 = atom.get_potential_energy() + atom.calc.get_reference_energy()
    calc.write('H.gpw')
    
    calc.set(hund=False)  # No hund rule for molecules
    
    molecule.set_calculator(calc)
    e2 = molecule.get_potential_energy() + molecule.calc.get_reference_energy()
    calc.write('H2.gpw')
    
    parprint('  hydrogen atom energy:     %5.2f eV' % e1, file=fd)
    parprint('  hydrogen molecule energy: %5.2f eV' % e2, file=fd)
    parprint('  atomization energy:       %5.2f eV' % (2 * e1 - e2), file=fd)

fd.close()
