#!/usr/bin/env gpaw-python

from __future__ import print_function
from math import sqrt

import numpy as np
from ase import Atoms, Atom
from ase.io import write, read
from ase.visualize import view

from gpaw import GPAW, PoissonSolver
from gpaw.xc.sic import SIC

xc = 'LDA'

a = 6.0
spacing = 0.15
mol = Atoms('H', magmoms=[1.0], cell=(a, a, a))
mol.center()

#this is the exact calculation which will yield the exact density and energy
dfa = SIC(xc=xc, xc_factor=1.0, coulomb_factor=1.0)
calc = GPAW(xc=dfa, spinpol=True, mode='fd', h=spacing, eigensolver='rmm-diis')

mol.set_calculator(calc)

En = mol.get_potential_energy() + calc.get_reference_energy()

Etn = En + mol.calc.get_xc_difference(xc=xc)
errorfunc = Etn - En

mol.calc.set(xc=xc)

Etnt = mol.get_potential_energy()
Etnt += mol.calc.get_reference_energy()

errordens = Etnt - Etn

print("total error: %14.8f, density error: %14.8f, functional error: %14.8f" %(Etnt - En, errordens, errorfunc))
