# Tutorial to the lecture "Advanced Simulation Methods"

In this tutorial we will work with [GPAW](https://wiki.fysik.dtu.dk/gpaw/) on a couple of advanced topics of density functional theory.
The main focus is on errors in GGA (generalized gradient approximation) type density functionals.
The students will work on two small sub-projects illustrating different kind of problems _haunting_ modern density functional approximations (DFAs).
At the end, students will wrap up their results and put them into context of density-driven and functional errors of DFAs ([KSB2013]).
They should also discuss some technical issues related to the specific calculations.

Most of the examples below are complete and should run with minor to no modification. 
This does not imply that you shouldn't look at them. In most cases information about the calculator is _NOT_ output, but might be useful (for visualization of electron densities, e.g.)

## Self-interaction correction at the example of two radicals - hydrogen atom and hydronium radical

GGA-type DFAs suffer from the so-called self-interaction error, meaning that a single electron spuriously repels itself (compare HF theory).
Due to this error GGA-type DFAs are not even capable of describing one of the simplest quantum chemical examples correctly: the hydrogen atom.
Hence, this is the first example that you will work on.

- check out the input examples under [hydrogen](tutorials/partI/hydrogen/)

- run the first hydrogen atom calculation ([H.py](tutorials/partI/hydrogen/H.py)) and examine the output energies for the different DFAs
    * What is the total energy and what should it be?
    * Compare the result to a Hartree-Fock (HF) calculation. In what relation are (in this special one-electron case) HF and SIC-DFT (self-interaction corrected DFT uses [PZ1981])?
    * How do the densities differ between the different methods?

- run the script with the dissection of the error into different components ([dissect.py](tutorials/partI/hydrogen/dissect.py))
    * Within the local density approximation, what is the dominant error? Does this change with different functional (classes)?

- run the [H2](tutorials/partI/hydrogen/H2.py) atomization example and examine how the energy changes with respect to the self-interaction correction used
    * How accurate do you think the calculations are 1 eV, 0.1 eV, 0.01 eV of your total energy?
    * Is the Perdew-Zunger self-interaction correction exact for many-electron systems? If yes/no, why?

The second example system is the dissociation of hydronium radical into a water molecule and a hydrogen atom.
The dissociation will be modelled as a hydrogen abstraction with and without nuclear relaxation, i.e., only the electronic structure is calculated upon change of the nuclear coordinates along O-H separation.
In the first step you will optimize, or _tune_, a density functional with respect to the IP theorem in DFT.

- Choose a reference DFA to compare your results against. Explain your choice.
- investigate convergence of the vertical ionization energy (VIE) with respect to grid spacing and vacuum size ([convergence.py](tutorials/partI/hydronium/convergence.py))
    * choose reasonably converged parameters for your further calculations
    
- Perform the IP-theorem _tuning_ (see [sic_tuning](tutorials/partI/hydronium/tuning.py))
- calculate the density-driven, functional, and overall errors compared to the MP2 result ([df_error.py](tutorials/partI/hydronium/df_error.py))

- Calculate the path for hydrogen atom dissociation from a hydronium radical with the DF with the lowest overall error ([reaction_tuned.py](tutorials/partI/hydronium/reaction_tuned.py))).
    * Why are the calculations based on the energy difference between H3O vs H2O and H, when no nuclear relaxation is taken into account?
    * What are the differences between your calculations and those in [UMJ2011], in terms of setup and calculation outcome?
    * What do you think is the major difference between the energy path in [UMJ2011] and the one obtained in your calculations?
    * What is the difference between the SIC used in [UMJ2011] and the one you used in your calculations (hydrogen atom)?
    * Where along the reaction path is the SIE the worst?
    
- Compare the energies obtained with your reference DFA to those of different approximations.
    * recalculate the potential energy along reaction with a GGA-type functional, and investigate the influence of nuclear relaxation ([reaction_gga.py](tutorials/partI/hydronium/reaction_gga.py))
        + what is the influence on the barrier height and on the relative energy of reactants and products?
        
## van-der-Waals type interactions and electronic structure of graphene/graphite

In this part of the tutorial you will be concerned with graphene and its electronic structure and the influence of inclusion of van-der-Waals type interactions into the DFA.
Starting from a benzene molecule you work your way up to a fully periodic graphene sheet(s).
You will compare the density of states, potential energy curves for intersheet separation and evaluate your results with respect to density-driven and functional errors.

- Investigate the change of electronic structure going from a benzene molecule to a graphene sheet, and to two graphite sheets.
- Evaluate the self-consistent and the perturbative vdW-DF corrections to the potential energy curves. Compare with empirical results from a pairwise-additive correction.

- as a first step examine density-driven and functional errors for a benzene dimer ([df_error.py](tutorials/partII/benzene/df_error.py))
    * the reference energy is taken as the interaction energy at minimum separation from literature (CCSD(T)) values ([SS2004])
    * Why do we not use the total energy (cnf. earlier hydronium example) as comparison?
    * What are the underlying assumptions for that? Are they valid?
    * What is the choice for calculating the reference energy? Does this make sense?
    * Note the difference between self-consistent and perturbative calculation
        + Is it worth doing the self-consisten calculation?

- calculate the dimer separation curve ([dimer.py](tutorials/partII/benzene/dimer.py))

- compare with a the semi-empirical method given in [TS2009], corresponding script: [ts09.py](tutorials/partII/benzene/ts09.py)
    * is it worth the effort calculating the fully self-consistent energy profile?
    * What is the difference between the methods in [TS2009] and the vdW-DF2 [HL2014]

- What are the difference between the calculations in [dimer.py](tutorials/partII/benzene/dimer.py) and [lcao.py](tutorials/partII/benzene/lcao.py)?

- calculate the density of states for the benzene monomer and dimer at the minimum energy separation (compare later to graphene/graphite)

- perform similar calculations, but fully self-consistent for the periodic graphene and graphite structures ([graphene.py](tutorials/partII/periodic_graph/graphene.py) and [graphite.py](tutorials/partII/periodic_graph/graphite.py))

- calculate density of states ([dos.py](tutorials/partII/dos.py)) and its projected equivalent ([projectdos.py](tutorials/partII/projectdos.py)) make sure you project onto all (relevant) atomic states to get the complete total DOS
    * How does this compare between graphene and graphite; benzene, and benzene dimer; benzene, and graphene


# References

[PZ1981](http://dx.doi.org/10.1103/PhysRevB.23.5048)

[SS2004](http://dx.doi.org/10.1021/jp0469517)

[TS2009](http://dx.doi.org/10.1103/PhysRevLett.102.073005)

[UMJ2011](http://dx.doi.org/10.1039/C1CP20764D)

[KSB2013](http://dx.doi.org/10.1103/PhysRevLett.111.073003)

[HL2014](http://arxiv.org/abs/1412.6827)
